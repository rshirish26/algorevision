package com.algo.practice.shirish.AlgoRevision.arrays;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FourNumberSum {

	public static void main(String[] args) {
		
		int []array = {7, 6, 4, -1, 1, 2};
		int targetSum = 16;
		
		// targetSum - (one + two + three)
		
		// store the sum of all the 2 numbers in the hashmap
		// do the sum of other to numbers and get the difference
		// if the difference is present in the hashmap the collect
		List<Integer[]> result = fourNumberSum(array, targetSum);
		System.out.println("Result " +result);
		
		
	}

	public static List<Integer[]> fourNumberSum(int[] array, int targetSum) {
		
		// define hashmap <Integer, List<Integer>>
		Map<Integer, List<Integer[]>> allValuesMap = new HashMap<Integer, List<Integer[]>>();
		// define a data structure which can store the qudrapulet
		List<Integer[]> qudraplet = new ArrayList<Integer[]>();
		// loop from 1 to end-1
		for(int i = 1; i < array.length -1; i++) {
		// loop from 2 to end
			for(int j =i+1; i < array.length; j++ )
			{
				// sum 1 and 2
				int total = array[i] +array[j];
				// diff = target - sum
				int difference = targetSum - total;
				// check if present in the hashmap
				if(allValuesMap.containsKey(difference))
				{   // if present -> add it into the hashmap
					for (Integer[] pair : allValuesMap.get(difference))
					{
						Integer [] completePair = {array[i], array[j], pair[0], pair[1]};
						qudraplet.add(completePair);
					}
				}
			}
			for(int k =0 ;k<i; k++)
			{
				int total = array[i] + array[k];
				Integer[] pair = {array[i], array[k]};
				if(!allValuesMap.containsKey(total))
				{
					List<Integer[]> pairGroup = new ArrayList<Integer[]>();
					pairGroup.add(pair);
					
					allValuesMap.put(total, pairGroup);
				}
				else {
					allValuesMap.get(total).add(pair);
				}
				
			}
		
		}
		// loop from 0 to 1
		// sum = 0 +1
		// if not present in the hashmap then add
		// if present then expand the list
		
	    return qudraplet;
	  }
	
}
