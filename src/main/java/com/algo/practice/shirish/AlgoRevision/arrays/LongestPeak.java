package com.algo.practice.shirish.AlgoRevision.arrays;

public class LongestPeak {

	public static void main(String[] args) {

		int array[] = { 1, 2, 3, 3, 4, 0, 10, 6, 5, -1, -3, 2, 3 };
		int result = longestPeak(array);
		System.out.println("Result :" + result);

	}

	public static int longestPeak(int[] array) {
		// initialize
		int longestPeak = 0;
		// iterate the array
		int i = 1;
		while (i < array.length - 1) {
			// find the current value is peak or not
			boolean isPeak = false;
			if (array[i] > array[i - 1] && array[i] > array[i + 1])
				isPeak = true;
			// if not peak => i++ and continue
			if (!isPeak) {
				i++;
				continue;
			}
			// while iterate(-) till everything is smaller than left = i-2
			int left = i - 2;
			while (left >= 0 && array[left] < array[left + 1])
				left--;
			// while iterate(+) till everything is greater than right = i+2
			int right = i + 2;
			while (right < array.length && array[right] > array[right - 1])
				right++;

			// calculate length
			int length = right - left - 1;
			// compare length with the longest peak
			if (length > longestPeak)
				longestPeak = length;
		}
		// return longest peak
		return longestPeak;
	}

}
