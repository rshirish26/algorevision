package com.algo.practice.shirish.AlgoRevision.arrays;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class LongestRange {

	public static void main(String[] args) {
		int[] array = { 1, 11, 3, 0, 15, 5, 2, 4, 10, 7, 12, 6 };
		int[] result = largestRange(array);
		System.out.println("Result : " + Arrays.toString(result));
	}

	public static int[] largestRange(int[] array) {
		//create a map for tracking
				Map<Integer, Boolean> tracking = new HashMap<Integer, Boolean>();
				int longestRange = 0;
				int [] bestRange = new int[2];
				//Initialize all the values of the map as true
				for(int value : array)
				{
					tracking.put(value, true);
				}
				// loop through the array to find out the largest range
				for(int value: array)
				{
					// check if the value is already traced then continue
					if(!tracking.get(value))
					{
						continue;
					}
					//mark the number as false
					tracking.put(value,false);
					//initialize the longest range and left and right values
					int currentRange =1;
					int left = value -1;
					int right = value +1;
					//loop till the incremented left value is present
					while(tracking.containsKey(left))
					{
						tracking.put(left, false);
						currentRange++;
						left--;
					}
					//loop till the decremented right value is present
					while(tracking.containsKey(right))
					{
						tracking.put(right, false);
						currentRange++;
						right++;
					}
					// compare current range with the longest range
					if(currentRange > longestRange)
					{
						longestRange = currentRange;
						bestRange = new int[] {left+1, right-1};
					}
					
				}
				

				return bestRange;
	}

}
