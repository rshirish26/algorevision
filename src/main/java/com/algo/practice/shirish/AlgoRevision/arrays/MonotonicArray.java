package com.algo.practice.shirish.AlgoRevision.arrays;

public class MonotonicArray {

	public static void main(String[] args) {
		int[] array = { -1, -5, -10, -1100, -1100, -1101, -1102, -9001 };
		boolean result = isMonotonic(array);
		System.out.println("Result :" + result);
	}

	public static boolean isMonotonic(int[] array) {

		// isIncreasting = true
		boolean isIncreasing = true;
		// isDecreasting = true
		boolean isDecreasing = true;
		// for loop starting from 1st element
		for (int start = 1; start < array.length; start++) {
			// if(array[start] < array[start -1]) isIncreasing = false
			if (array[start] < array[start - 1])
				isIncreasing = false;
			// if(array[start] > array[start-1]) isDecreasing = false
			if (array[start] > array[start - 1])
				isDecreasing = false;
		}
		// return isDecreasing || isIncreasing
		return isDecreasing || isIncreasing;
	}
}
