package com.algo.practice.shirish.AlgoRevision.arrays;

import java.util.Arrays;
import java.util.List;

public class MoveTheElementToTheEnd {

	public static void main(String args[]) {

		List<Integer> array = Arrays.asList(2, 1, 2, 2, 2, 3, 4, 2);
		int toMove = 2;
		List<Integer> result = moveElementToEnd(array, toMove);
		System.out.println(" Result " + result);

	}

	public static List<Integer> moveElementToEnd(List<Integer> array, int toMove) {

		// initialize start and end
		int start = 0;
		int end = array.size() - 1;
		// loop till start < end
		while (start < end)
		// loop till start < end and end == toMove => j--
		{
			while (start < end && array.get(end) == toMove)
				end--;
			// if(start == toMove) swap (i, j, array)
			if (array.get(start) == toMove)
				// if(start == toMove) swap (i, j, array)
				swap(start, end, array);
			start++;

		}
		// return
		return array;
	}

	private static void swap(int start, int end, List<Integer> array) {
		int temp = array.get(end);
		array.set(end, array.get(start));
		array.set(start, temp);
	}

}
