package com.algo.practice.shirish.AlgoRevision.arrays;

import java.util.ArrayList;
import java.util.List;

public class SprialTraverse {

	public static void main(String[] args) {

		// [[1, 2, 3],
		// [12, 13, 4],
		// [11, 14, 5],
		// [10, 15, 6],
		// [9, 8, 7]]
		int array[][] = { { 1, 2, 3, 4 }, { 12, 13, 14, 5 }, { 11, 16, 15, 6 }, { 10, 9, 8, 7 } };
		List<Integer> result = spiralTraverse(array);
		System.out.println("Result :" + result);
	}

	public static List<Integer> spiralTraverse(int[][] array) {
		// Write your code here

		if (array.length == 0)
			return new ArrayList<Integer>();
		// initialize

		int startRow = 0;
		int endRow = array.length - 1;
		int startCol = 0;
		int endCol = array[0].length - 1;
		List<Integer> result = new ArrayList<Integer>();
		// loop till
		while (startRow < endRow && startCol < endCol) {
			// Trace the first Row
			for (int col = startCol; col <= endCol; col++)
				result.add(array[startRow][col]);
			// Trace the last column
			for (int row = startRow + 1; row <= endRow; row++)
				result.add(array[row][endCol]);
			// Trace the last Row
			for (int col = endCol - 1; col >= startCol; col--) {
				if (startRow == endRow)
					break;
				result.add(array[endRow][col]);
			}
			// Trace the first Col
			for (int row = endRow - 1; row > startRow; row--) {
				if (startCol == endCol)
					break;
				result.add(array[row][startCol]);
			}
			startRow++;
			endRow--;
			startCol++;
			endCol--;

		}
		return result;
	}

}
