package com.algo.practice.shirish.AlgoRevision.arrays;

import java.util.Arrays;

public class TwoNumberSum {

	public static void main(String[] args) {

		int array[] = { 4, 6, 1 };
		int targetSum = 5;
		int result[] = twoNumberSum(array, targetSum);

		System.out.println("Final Result : " + Arrays.toString(result));
	}

	public static int[] twoNumberSum(int[] array, int targetSum) {
		// sort the array
		Arrays.sort(array);
		int startPointer = 0;
		int endPointer = array.length - 1;
		// loop till start < end
		while (startPointer < endPointer) {
			// if(start + end > targetSum) => start++
			if (array[startPointer] + array[endPointer] < targetSum) {
				startPointer++;
			} // else if (start + end < targetSum) => end--
			else if (array[startPointer] + array[endPointer] > targetSum) {
				endPointer--;
			} // if(start + end == targetSum) return 2 numbers
			else if (array[startPointer] + array[endPointer] == targetSum) {
				return new int[] { array[startPointer], array[endPointer] };
			}
		}

		return new int[0];
	}

}
