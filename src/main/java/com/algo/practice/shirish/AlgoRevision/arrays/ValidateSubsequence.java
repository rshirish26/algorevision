package com.algo.practice.shirish.AlgoRevision.arrays;

import java.util.Arrays;
import java.util.List;

public class ValidateSubsequence {

	public static void main(String[] args) {

		List<Integer> array = Arrays.asList(5, 1, 22, 25, 6, -1, 8, 10);
		List<Integer> sequence = Arrays.asList(22, 25, 6);
		boolean result = isValidSubsequence(array, sequence);
		System.out.println("Result :" + result);

	}

	public static boolean isValidSubsequence(List<Integer> array, List<Integer> sequence) {

		// startIdx = 0
		int startIdx = 0;
		// iterate the array
		for (Integer value : array) {
			// if (startIdx == sequence.size) break
			if (startIdx == sequence.size())
				break;
			// if (value == sequence.get(startIdx)) startIdx++
			if (sequence.get(startIdx).equals(value))
				startIdx++;
		}
		// return startIdx == sequ.size
		return startIdx == sequence.size();
	}

}
