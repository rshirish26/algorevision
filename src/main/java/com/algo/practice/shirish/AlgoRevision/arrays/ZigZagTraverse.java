package com.algo.practice.shirish.AlgoRevision.arrays;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ZigZagTraverse {

	public static void main(String[] args) {
		
		List<List<Integer>> matrix = new ArrayList<List<Integer>>() ;
		matrix.add(new ArrayList<Integer>(Arrays.asList(1, 3, 4, 10)));
		matrix.add(new ArrayList<Integer>(Arrays.asList(2, 5, 9, 11)));
		matrix.add(new ArrayList<Integer>(Arrays.asList(6, 8, 12, 15)));
		matrix.add(new ArrayList<Integer>(Arrays.asList(7, 13, 14, 16)));
		
//		[[1, 3, 4, 10], 
//		 [2, 5, 9, 11], 
//		 [6, 8, 12, 15], 
//		 [7, 13, 14, 16]]

	}
	
	public static List<Integer> zigzagTraverse(List<List<Integer>> array) {
		// initialize height and width | col and row
		int height = array.size()-1;
		int width = array.get(0).size()-1;
		int row = 0;
		int col = 0;
		// storing result
		List<Integer> result = new ArrayList<Integer>();
		// initially going down
		boolean  goingDown = true;
		// loop till we are not out of range
		while(outOfRange(height, width, row, col))
		{
		}

		
		
		
	    
	    return new ArrayList<Integer>();
	  }

	private static boolean outOfRange(int height, int width, int row, int col) {
		
		return (row < 0 || row > height || col < 0 || col > width );
	}

}
