package com.algo.practice.shirish.AlgoRevision.graphs;

public class SingleCycle {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] input = { 2, 3, 1, -4, -4, 2 };
		boolean result = hasSingleCycle(input);
		System.out.println("Result :" + result);
	}

	public static boolean hasSingleCycle(int[] input) {

		int currentIdx = 0;
		int numberOfElementsVisited = 0;

		while (numberOfElementsVisited < input.length) {
			if (numberOfElementsVisited > 0 && currentIdx == 0)
				return false;
			currentIdx = getNextIdx(currentIdx, input);
			numberOfElementsVisited++;
		}

		return currentIdx == 0;
	}
	
	private static int getNextIdx(int currentIdx, int[] input) {
		int jump = input[currentIdx];
		int nextIndex = (currentIdx + jump) % input.length;
		return (nextIndex >= 0) ? nextIndex : input.length + nextIndex;
	}
}
