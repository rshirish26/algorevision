package com.algo.practice.shirish.AlgoRevision.string;

import java.util.ArrayList;
import java.util.List;

public class UnderscorifyString {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
      String str = "testthis is a testtest to see if testestest it works";
      String subString = "test";
      underscorifySubstring(str, subString) ;
	}
	
	public static String underscorifySubstring(String str, String substring) {
	    //define location
		List<Integer[]> collapsedLocations = new ArrayList<Integer[]>();
		//get Location of Substring
		collapsedLocations = getCollapsedLocations(getLocationOfSubstring(str, substring));
		
		
	
	    return "";
	  }

	private static List<Integer[]> getCollapsedLocations(List<Integer[]> locationOfSubstring) {
	     //if the locations size is zero return empty List
		if(locationOfSubstring.size() == 0)
		{
			return new ArrayList<Integer[]>();
		}
		//store the first value in 
		List<Integer[]> newLocations = new ArrayList<Integer[]>();
		Integer[] previous = locationOfSubstring.get(0);
		//iterate the locations
		newLocations.add(previous);
		for(Integer[] current :locationOfSubstring)
		{ 
			// if current 0 <= pervious 1 then prev 1 = current 1
			if (current[0] <= previous[1])
			{
				previous[1] = current[1];
				newLocations.add(current);
				previous = current;
			}
			else 
			{
				newLocations.add(current);
				previous = current;
			}
		}
		
		
		return newLocations;
	}

	private static List<Integer[]> getLocationOfSubstring(String str, String substring) {
		// define pointer to track the location
		int index = 0;
		// store the locations here
		List<Integer[]> locations = new ArrayList<Integer[]>();
				// loop till index is smaller than str length
		while(index < str.length())
		{
			// store the index of the first instance of substring starting fron index
			int firstInstanceIndex = str.indexOf(substring, index);
			// store it in the locations list
			if (firstInstanceIndex!=-1)
			{
			locations.add(new Integer[] {firstInstanceIndex , firstInstanceIndex + substring.length() });
			index = firstInstanceIndex +1;
			}
			else 
			{
				break;
			}
		}		
		return locations;
	}

}
